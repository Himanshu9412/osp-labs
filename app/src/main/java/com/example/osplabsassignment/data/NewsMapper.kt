package com.example.osplabsassignment.data

import com.example.osplabsassignment.domain.model.Article

object NewsMapper {
    fun List<Article>.transform(): List<NewsData> {
        return map {
            NewsData(
                author = it.author,
                content = it.content,
                description = it.description,
                publishedAt = it.publishedAt,
                title = it.title,
                url = it.url,
                urlToImage = it.urlToImage
            )
        }
    }
}