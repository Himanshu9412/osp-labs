package com.example.osplabsassignment.domain.api

import com.example.osplabsassignment.domain.model.NewsDataResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface NewsApi {
    @GET("top-headlines")
    suspend fun getTopHeadlines(@Query("Country") country: String): Response<NewsDataResponse>
}