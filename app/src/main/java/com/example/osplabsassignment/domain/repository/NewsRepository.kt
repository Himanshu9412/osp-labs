package com.example.osplabsassignment.domain.repository

import com.example.osplabsassignment.domain.model.NewsDataResponse
import retrofit2.Response

interface NewsRepository {
    suspend fun getTopHeadlines(country: String): Response<NewsDataResponse>
}