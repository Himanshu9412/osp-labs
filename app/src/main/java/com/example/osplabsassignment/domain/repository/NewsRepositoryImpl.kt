package com.example.osplabsassignment.domain.repository

import com.example.osplabsassignment.domain.api.NewsApi
import com.example.osplabsassignment.domain.model.NewsDataResponse
import retrofit2.Response
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val newsApi: NewsApi
) : NewsRepository {
    override suspend fun getTopHeadlines(country: String): Response<NewsDataResponse> {
        return newsApi.getTopHeadlines(country)
    }

}