package com.example.osplabsassignment.ui.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.osplabsassignment.core.NetworkHelper
import com.example.osplabsassignment.core.UiModel
import com.example.osplabsassignment.data.NewsData
import com.example.osplabsassignment.data.NewsMapper.transform
import com.example.osplabsassignment.domain.repository.NewsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsViewModel @Inject constructor(
    private val newsRepository: NewsRepository,
    private val networkHelper: NetworkHelper
) : ViewModel() {
    private val newsList = MutableLiveData<UiModel<List<NewsData>>>()
    val newsData: LiveData<UiModel<List<NewsData>>>
        get() = newsList

    init {
        fetchData("us")
    }

    private fun fetchData(country: String) {
        viewModelScope.launch(Dispatchers.IO) {
            newsList.postValue(UiModel.loading())

            if (networkHelper.isConnected()) {
                newsRepository.getTopHeadlines(country).let {
                    if (it.isSuccessful) {
                        val data = it.body()?.articles?.transform()
                        newsList.postValue(UiModel.success(data))
                    } else {
                        newsList.postValue(UiModel.error(it.errorBody().toString(), null))
                    }
                }
            } else {
                newsList.postValue(UiModel.error("Please check your internet connection!", null))
            }
        }
    }
}