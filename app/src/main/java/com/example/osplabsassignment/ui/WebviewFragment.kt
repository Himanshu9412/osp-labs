package com.example.osplabsassignment.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.osplabsassignment.core.WebviewHelper
import com.example.osplabsassignment.databinding.FragmentWebviewBinding

class WebviewFragment : Fragment() {
    lateinit var binding: FragmentWebviewBinding
    private val args: WebviewFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWebviewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.apply {
            WebviewHelper.shouldOverrideUrlLoading(webview, args.linkUrl)
        }
    }
}