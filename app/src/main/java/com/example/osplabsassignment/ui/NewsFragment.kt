package com.example.osplabsassignment.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.osplabsassignment.R
import com.example.osplabsassignment.core.Status
import com.example.osplabsassignment.core.UiHelper.showToast
import com.example.osplabsassignment.core.UiHelper.toggleProgressBar
import com.example.osplabsassignment.data.NewsData
import com.example.osplabsassignment.databinding.FragmentNewsBinding
import com.example.osplabsassignment.ui.adapter.ItemNewsAdapter
import com.example.osplabsassignment.ui.viewModel.NewsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class NewsFragment : Fragment() {
    private lateinit var binding: FragmentNewsBinding
    val viewModel by viewModels<NewsViewModel>()
    private var adapter: ItemNewsAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsBinding.inflate(inflater, container, false)
        val view = binding.root
        val layoutManager = LinearLayoutManager(requireContext())
        binding.rvNewsList.layoutManager = layoutManager
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.appbar.toolbar.title = getString(R.string.osp_news)

        viewModel.newsData.observe(viewLifecycleOwner) {
            binding.progressBar.toggleProgressBar(it.status)
            when (it.status) {
                Status.ERROR -> it.message?.showToast(requireContext())
                Status.SUCCESS -> setupRecyclerView(it.data ?: emptyList())
                else -> Unit
            }
        }
    }

    private fun setupRecyclerView(data: List<NewsData>) {
        val onItemClick = { position: Int ->
            val action =
                NewsFragmentDirections.actionNewsFragmentToNewsDetailFragment(data[position])
            findNavController().navigate(action)
        }

        adapter = ItemNewsAdapter(requireContext(), onItemClick)
        binding.rvNewsList.adapter = adapter
        adapter?.newsList = data
    }
}