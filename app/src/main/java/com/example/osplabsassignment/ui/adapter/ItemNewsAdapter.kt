package com.example.osplabsassignment.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.osplabsassignment.core.AppDateFormat
import com.example.osplabsassignment.data.NewsData
import com.example.osplabsassignment.databinding.NewsItemBinding
import com.squareup.picasso.Picasso

class ItemNewsAdapter(val context: Context, val onItemClick: (position: Int) -> Unit) :
    RecyclerView.Adapter<ItemNewsAdapter.ViewHolder>() {

    private var data: List<NewsData> = listOf()
    var newsList = listOf<NewsData>()
        set(value) {
            field = value
            data = newsList
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = NewsItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val newsData = data[position]
        holder.binding.apply {
            tvTitle.text = newsData.title
            tvDate.text = AppDateFormat.format(newsData.publishedAt.toString())
            Picasso.get().load(newsData.urlToImage).into(imgThumbnail)

            cvNewsItem.setOnClickListener {
                onItemClick(position)
            }
        }
    }

    override fun getItemCount() = data.size

    class ViewHolder(val binding: NewsItemBinding) : RecyclerView.ViewHolder(binding.root)
}