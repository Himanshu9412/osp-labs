package com.example.osplabsassignment.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.osplabsassignment.core.AppDateFormat
import com.example.osplabsassignment.data.NewsData
import com.example.osplabsassignment.databinding.FragmentNewsDetailBinding
import com.squareup.picasso.Picasso

class NewsDetailFragment : Fragment() {
    lateinit var binding: FragmentNewsDetailBinding
    private val args: NewsDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView(args.itemDetail)
    }

    private fun setupView(itemDetail: NewsData) {
        binding.apply {
            Picasso.get().load(itemDetail.urlToImage).into(imgThumbnail)
            tvTitle.text = itemDetail.title
            tvAurthor.text = itemDetail.author
            tvDate.text = AppDateFormat.format(itemDetail.publishedAt.toString())
            tvContent.text = itemDetail.content

            fabLink.setOnClickListener {
                val action = NewsDetailFragmentDirections.actionNewsDetailFragmentToWebviewFragment(
                    itemDetail.url ?: "No Data Available"
                )
                findNavController().navigate(action)
            }
        }
    }
}