package com.example.osplabsassignment.core

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object AppDateFormat {
    private const val JSON_DATE_1 = "dd-MM-yyyy hh:mm"
    private val DF_JSON_DATE1 = SimpleDateFormat(JSON_DATE_1, Locale.ENGLISH)

    private const val JSON_DATE_2 = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    private val DF_JSON_DATE2: DateFormat = SimpleDateFormat(JSON_DATE_2, Locale.ENGLISH)

    fun format(givenDate: String): String {
        val input = DF_JSON_DATE2
        val output = DF_JSON_DATE1

        var date: Date? = null
        try {
            date = input.parse(givenDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return output.format(date ?: "")
    }
}