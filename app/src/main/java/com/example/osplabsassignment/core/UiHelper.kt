package com.example.osplabsassignment.core

import android.content.Context
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.view.isVisible
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

object UiHelper {
    fun ProgressBar.toggleProgressBar(status: Status) {
        isVisible = status is Status.LOADING
    }

    fun String.showToast(context: Context){
        Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
    }
}