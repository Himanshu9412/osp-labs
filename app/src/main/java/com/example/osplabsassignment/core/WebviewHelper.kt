package com.example.osplabsassignment.core

import android.webkit.WebView
import android.webkit.WebViewClient

object WebviewHelper : WebViewClient() {
    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        view.loadUrl(url)
        return true
    }
}