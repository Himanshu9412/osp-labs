package com.example.osplabsassignment.core;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0003\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lcom/example/osplabsassignment/core/Status;", "", "()V", "ERROR", "LOADING", "SUCCESS", "Lcom/example/osplabsassignment/core/Status$LOADING;", "Lcom/example/osplabsassignment/core/Status$SUCCESS;", "Lcom/example/osplabsassignment/core/Status$ERROR;", "app_debug"})
public abstract class Status {
    
    private Status() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/osplabsassignment/core/Status$LOADING;", "Lcom/example/osplabsassignment/core/Status;", "()V", "app_debug"})
    public static final class LOADING extends com.example.osplabsassignment.core.Status {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.osplabsassignment.core.Status.LOADING INSTANCE = null;
        
        private LOADING() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/osplabsassignment/core/Status$SUCCESS;", "Lcom/example/osplabsassignment/core/Status;", "()V", "app_debug"})
    public static final class SUCCESS extends com.example.osplabsassignment.core.Status {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.osplabsassignment.core.Status.SUCCESS INSTANCE = null;
        
        private SUCCESS() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/osplabsassignment/core/Status$ERROR;", "Lcom/example/osplabsassignment/core/Status;", "()V", "app_debug"})
    public static final class ERROR extends com.example.osplabsassignment.core.Status {
        @org.jetbrains.annotations.NotNull()
        public static final com.example.osplabsassignment.core.Status.ERROR INSTANCE = null;
        
        private ERROR() {
            super();
        }
    }
}