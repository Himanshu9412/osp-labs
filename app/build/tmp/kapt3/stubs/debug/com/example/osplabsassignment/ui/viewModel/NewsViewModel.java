package com.example.osplabsassignment.ui.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.osplabsassignment.core.NetworkHelper;
import com.example.osplabsassignment.core.UiModel;
import com.example.osplabsassignment.data.NewsData;
import com.example.osplabsassignment.domain.repository.NewsRepository;
import dagger.hilt.android.lifecycle.HiltViewModel;
import kotlinx.coroutines.Dispatchers;
import javax.inject.Inject;

@dagger.hilt.android.lifecycle.HiltViewModel()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0007\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\f\u0010\rR \u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/example/osplabsassignment/ui/viewModel/NewsViewModel;", "Landroidx/lifecycle/ViewModel;", "newsRepository", "Lcom/example/osplabsassignment/domain/repository/NewsRepository;", "networkHelper", "Lcom/example/osplabsassignment/core/NetworkHelper;", "(Lcom/example/osplabsassignment/domain/repository/NewsRepository;Lcom/example/osplabsassignment/core/NetworkHelper;)V", "newsData", "Landroidx/lifecycle/LiveData;", "Lcom/example/osplabsassignment/core/UiModel;", "", "Lcom/example/osplabsassignment/data/NewsData;", "getNewsData", "()Landroidx/lifecycle/LiveData;", "newsList", "Landroidx/lifecycle/MutableLiveData;", "fetchData", "", "country", "", "app_debug"})
public final class NewsViewModel extends androidx.lifecycle.ViewModel {
    private final com.example.osplabsassignment.domain.repository.NewsRepository newsRepository = null;
    private final com.example.osplabsassignment.core.NetworkHelper networkHelper = null;
    private final androidx.lifecycle.MutableLiveData<com.example.osplabsassignment.core.UiModel<java.util.List<com.example.osplabsassignment.data.NewsData>>> newsList = null;
    
    @javax.inject.Inject()
    public NewsViewModel(@org.jetbrains.annotations.NotNull()
    com.example.osplabsassignment.domain.repository.NewsRepository newsRepository, @org.jetbrains.annotations.NotNull()
    com.example.osplabsassignment.core.NetworkHelper networkHelper) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.example.osplabsassignment.core.UiModel<java.util.List<com.example.osplabsassignment.data.NewsData>>> getNewsData() {
        return null;
    }
    
    private final void fetchData(java.lang.String country) {
    }
}