package com.example.osplabsassignment.core;

import android.content.Context;
import android.widget.ProgressBar;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007J\u0012\u0010\b\u001a\u00020\u0004*\u00020\t2\u0006\u0010\n\u001a\u00020\u000b\u00a8\u0006\f"}, d2 = {"Lcom/example/osplabsassignment/core/UiHelper;", "", "()V", "showToast", "", "", "context", "Landroid/content/Context;", "toggleProgressBar", "Landroid/widget/ProgressBar;", "status", "Lcom/example/osplabsassignment/core/Status;", "app_debug"})
public final class UiHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.osplabsassignment.core.UiHelper INSTANCE = null;
    
    private UiHelper() {
        super();
    }
    
    public final void toggleProgressBar(@org.jetbrains.annotations.NotNull()
    android.widget.ProgressBar $this$toggleProgressBar, @org.jetbrains.annotations.NotNull()
    com.example.osplabsassignment.core.Status status) {
    }
    
    public final void showToast(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$showToast, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
}