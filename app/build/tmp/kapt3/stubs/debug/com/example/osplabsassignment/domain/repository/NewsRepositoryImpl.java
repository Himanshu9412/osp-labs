package com.example.osplabsassignment.domain.repository;

import com.example.osplabsassignment.domain.api.NewsApi;
import com.example.osplabsassignment.domain.model.NewsDataResponse;
import retrofit2.Response;
import javax.inject.Inject;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\tH\u0096@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000b"}, d2 = {"Lcom/example/osplabsassignment/domain/repository/NewsRepositoryImpl;", "Lcom/example/osplabsassignment/domain/repository/NewsRepository;", "newsApi", "Lcom/example/osplabsassignment/domain/api/NewsApi;", "(Lcom/example/osplabsassignment/domain/api/NewsApi;)V", "getTopHeadlines", "Lretrofit2/Response;", "Lcom/example/osplabsassignment/domain/model/NewsDataResponse;", "country", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "app_debug"})
public final class NewsRepositoryImpl implements com.example.osplabsassignment.domain.repository.NewsRepository {
    private final com.example.osplabsassignment.domain.api.NewsApi newsApi = null;
    
    @javax.inject.Inject()
    public NewsRepositoryImpl(@org.jetbrains.annotations.NotNull()
    com.example.osplabsassignment.domain.api.NewsApi newsApi) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.Object getTopHeadlines(@org.jetbrains.annotations.NotNull()
    java.lang.String country, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super retrofit2.Response<com.example.osplabsassignment.domain.model.NewsDataResponse>> continuation) {
        return null;
    }
}