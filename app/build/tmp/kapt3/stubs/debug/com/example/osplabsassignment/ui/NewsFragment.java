package com.example.osplabsassignment.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.osplabsassignment.R;
import com.example.osplabsassignment.core.Status;
import com.example.osplabsassignment.data.NewsData;
import com.example.osplabsassignment.databinding.FragmentNewsBinding;
import com.example.osplabsassignment.ui.adapter.ItemNewsAdapter;
import com.example.osplabsassignment.ui.viewModel.NewsViewModel;
import dagger.hilt.android.AndroidEntryPoint;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u001a\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016J\u0016\u0010 \u001a\u00020\u001e2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020#0\"H\u0002J\b\u0010$\u001a\u00020\u001eH\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00108FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006%"}, d2 = {"Lcom/example/osplabsassignment/ui/NewsFragment;", "Landroidx/fragment/app/Fragment;", "()V", "adapter", "Lcom/example/osplabsassignment/ui/adapter/ItemNewsAdapter;", "getAdapter", "()Lcom/example/osplabsassignment/ui/adapter/ItemNewsAdapter;", "setAdapter", "(Lcom/example/osplabsassignment/ui/adapter/ItemNewsAdapter;)V", "binding", "Lcom/example/osplabsassignment/databinding/FragmentNewsBinding;", "getBinding", "()Lcom/example/osplabsassignment/databinding/FragmentNewsBinding;", "setBinding", "(Lcom/example/osplabsassignment/databinding/FragmentNewsBinding;)V", "viewModel", "Lcom/example/osplabsassignment/ui/viewModel/NewsViewModel;", "getViewModel", "()Lcom/example/osplabsassignment/ui/viewModel/NewsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "setupRecyclerView", "data", "", "Lcom/example/osplabsassignment/data/NewsData;", "setupView", "app_debug"})
@dagger.hilt.android.AndroidEntryPoint()
public final class NewsFragment extends androidx.fragment.app.Fragment {
    public com.example.osplabsassignment.databinding.FragmentNewsBinding binding;
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.Nullable()
    private com.example.osplabsassignment.ui.adapter.ItemNewsAdapter adapter;
    private java.util.HashMap _$_findViewCache;
    
    public NewsFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.osplabsassignment.databinding.FragmentNewsBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.example.osplabsassignment.databinding.FragmentNewsBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.osplabsassignment.ui.viewModel.NewsViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.osplabsassignment.ui.adapter.ItemNewsAdapter getAdapter() {
        return null;
    }
    
    public final void setAdapter(@org.jetbrains.annotations.Nullable()
    com.example.osplabsassignment.ui.adapter.ItemNewsAdapter p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void setupView() {
    }
    
    private final void setupRecyclerView(java.util.List<com.example.osplabsassignment.data.NewsData> data) {
    }
}