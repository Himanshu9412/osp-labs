package com.example.osplabsassignment.data;

import com.example.osplabsassignment.domain.model.Article;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004*\b\u0012\u0004\u0012\u00020\u00060\u0004\u00a8\u0006\u0007"}, d2 = {"Lcom/example/osplabsassignment/data/NewsMapper;", "", "()V", "transform", "", "Lcom/example/osplabsassignment/data/NewsData;", "Lcom/example/osplabsassignment/domain/model/Article;", "app_debug"})
public final class NewsMapper {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.osplabsassignment.data.NewsMapper INSTANCE = null;
    
    private NewsMapper() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.osplabsassignment.data.NewsData> transform(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.osplabsassignment.domain.model.Article> $this$transform) {
        return null;
    }
}