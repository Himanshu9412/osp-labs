package com.example.osplabsassignment.core;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/example/osplabsassignment/core/AppDateFormat;", "", "()V", "DF_JSON_DATE1", "Ljava/text/SimpleDateFormat;", "DF_JSON_DATE2", "Ljava/text/DateFormat;", "JSON_DATE_1", "", "JSON_DATE_2", "format", "givenDate", "app_debug"})
public final class AppDateFormat {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.osplabsassignment.core.AppDateFormat INSTANCE = null;
    private static final java.lang.String JSON_DATE_1 = "dd-MM-yyyy hh:mm";
    private static final java.text.SimpleDateFormat DF_JSON_DATE1 = null;
    private static final java.lang.String JSON_DATE_2 = "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'";
    private static final java.text.DateFormat DF_JSON_DATE2 = null;
    
    private AppDateFormat() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String format(@org.jetbrains.annotations.NotNull()
    java.lang.String givenDate) {
        return null;
    }
}