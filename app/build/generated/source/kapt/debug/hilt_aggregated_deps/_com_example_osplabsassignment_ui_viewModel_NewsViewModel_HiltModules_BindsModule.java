package hilt_aggregated_deps;

import dagger.hilt.processor.internal.aggregateddeps.AggregatedDeps;

/**
 * This class should only be referenced by generated code!This class aggregates information across multiple compilations.
 */
@AggregatedDeps(
    components = "dagger.hilt.android.components.ViewModelComponent",
    modules = "com.example.osplabsassignment.ui.viewModel.NewsViewModel_HiltModules.BindsModule"
)
public class _com_example_osplabsassignment_ui_viewModel_NewsViewModel_HiltModules_BindsModule {
}
