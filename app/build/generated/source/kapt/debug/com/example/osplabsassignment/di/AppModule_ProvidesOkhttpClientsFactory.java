// Generated by Dagger (https://dagger.dev).
package com.example.osplabsassignment.di;

import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvidesOkhttpClientsFactory implements Factory<OkHttpClient> {
  private final AppModule module;

  public AppModule_ProvidesOkhttpClientsFactory(AppModule module) {
    this.module = module;
  }

  @Override
  public OkHttpClient get() {
    return providesOkhttpClients(module);
  }

  public static AppModule_ProvidesOkhttpClientsFactory create(AppModule module) {
    return new AppModule_ProvidesOkhttpClientsFactory(module);
  }

  public static OkHttpClient providesOkhttpClients(AppModule instance) {
    return Preconditions.checkNotNullFromProvides(instance.providesOkhttpClients());
  }
}
