// Generated by Dagger (https://dagger.dev).
package com.example.osplabsassignment.di;

import com.example.osplabsassignment.domain.repository.NewsRepository;
import com.example.osplabsassignment.domain.repository.NewsRepositoryImpl;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppModule_ProvidesNewsRepositoryFactory implements Factory<NewsRepository> {
  private final AppModule module;

  private final Provider<NewsRepositoryImpl> newsRepoImplProvider;

  public AppModule_ProvidesNewsRepositoryFactory(AppModule module,
      Provider<NewsRepositoryImpl> newsRepoImplProvider) {
    this.module = module;
    this.newsRepoImplProvider = newsRepoImplProvider;
  }

  @Override
  public NewsRepository get() {
    return providesNewsRepository(module, newsRepoImplProvider.get());
  }

  public static AppModule_ProvidesNewsRepositoryFactory create(AppModule module,
      Provider<NewsRepositoryImpl> newsRepoImplProvider) {
    return new AppModule_ProvidesNewsRepositoryFactory(module, newsRepoImplProvider);
  }

  public static NewsRepository providesNewsRepository(AppModule instance,
      NewsRepositoryImpl newsRepoImpl) {
    return Preconditions.checkNotNullFromProvides(instance.providesNewsRepository(newsRepoImpl));
  }
}
