package com.example.osplabsassignment.ui;

import dagger.hilt.InstallIn;
import dagger.hilt.android.components.FragmentComponent;
import dagger.hilt.codegen.OriginatingElement;
import dagger.hilt.internal.GeneratedEntryPoint;

@OriginatingElement(
    topLevelClass = NewsFragment.class
)
@GeneratedEntryPoint
@InstallIn(FragmentComponent.class)
public interface NewsFragment_GeneratedInjector {
  void injectNewsFragment(NewsFragment newsFragment);
}
