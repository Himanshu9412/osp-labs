package com.example.osplabsassignment.ui

import android.os.Bundle
import androidx.navigation.NavDirections
import com.example.osplabsassignment.R
import kotlin.Int
import kotlin.String

public class NewsDetailFragmentDirections private constructor() {
  private data class ActionNewsDetailFragmentToWebviewFragment(
    public val linkUrl: String
  ) : NavDirections {
    public override val actionId: Int = R.id.action_newsDetailFragment_to_webviewFragment

    public override val arguments: Bundle
      get() {
        val result = Bundle()
        result.putString("linkUrl", this.linkUrl)
        return result
      }
  }

  public companion object {
    public fun actionNewsDetailFragmentToWebviewFragment(linkUrl: String): NavDirections =
        ActionNewsDetailFragmentToWebviewFragment(linkUrl)
  }
}
