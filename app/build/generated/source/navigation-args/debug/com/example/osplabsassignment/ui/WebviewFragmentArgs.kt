package com.example.osplabsassignment.ui

import android.os.Bundle
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import java.lang.IllegalArgumentException
import kotlin.String
import kotlin.jvm.JvmStatic

public data class WebviewFragmentArgs(
  public val linkUrl: String
) : NavArgs {
  public fun toBundle(): Bundle {
    val result = Bundle()
    result.putString("linkUrl", this.linkUrl)
    return result
  }

  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    result.set("linkUrl", this.linkUrl)
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): WebviewFragmentArgs {
      bundle.setClassLoader(WebviewFragmentArgs::class.java.classLoader)
      val __linkUrl : String?
      if (bundle.containsKey("linkUrl")) {
        __linkUrl = bundle.getString("linkUrl")
        if (__linkUrl == null) {
          throw IllegalArgumentException("Argument \"linkUrl\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"linkUrl\" is missing and does not have an android:defaultValue")
      }
      return WebviewFragmentArgs(__linkUrl)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): WebviewFragmentArgs {
      val __linkUrl : String?
      if (savedStateHandle.contains("linkUrl")) {
        __linkUrl = savedStateHandle["linkUrl"]
        if (__linkUrl == null) {
          throw IllegalArgumentException("Argument \"linkUrl\" is marked as non-null but was passed a null value")
        }
      } else {
        throw IllegalArgumentException("Required argument \"linkUrl\" is missing and does not have an android:defaultValue")
      }
      return WebviewFragmentArgs(__linkUrl)
    }
  }
}
