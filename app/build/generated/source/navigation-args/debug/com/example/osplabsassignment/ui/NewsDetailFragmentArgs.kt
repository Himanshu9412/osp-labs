package com.example.osplabsassignment.ui

import android.os.Bundle
import android.os.Parcelable
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NavArgs
import com.example.osplabsassignment.`data`.NewsData
import java.io.Serializable
import java.lang.IllegalArgumentException
import java.lang.UnsupportedOperationException
import kotlin.Suppress
import kotlin.jvm.JvmStatic

public data class NewsDetailFragmentArgs(
  public val itemDetail: NewsData
) : NavArgs {
  @Suppress("CAST_NEVER_SUCCEEDS")
  public fun toBundle(): Bundle {
    val result = Bundle()
    if (Parcelable::class.java.isAssignableFrom(NewsData::class.java)) {
      result.putParcelable("item_detail", this.itemDetail as Parcelable)
    } else if (Serializable::class.java.isAssignableFrom(NewsData::class.java)) {
      result.putSerializable("item_detail", this.itemDetail as Serializable)
    } else {
      throw UnsupportedOperationException(NewsData::class.java.name +
          " must implement Parcelable or Serializable or must be an Enum.")
    }
    return result
  }

  @Suppress("CAST_NEVER_SUCCEEDS")
  public fun toSavedStateHandle(): SavedStateHandle {
    val result = SavedStateHandle()
    if (Parcelable::class.java.isAssignableFrom(NewsData::class.java)) {
      result.set("item_detail", this.itemDetail as Parcelable)
    } else if (Serializable::class.java.isAssignableFrom(NewsData::class.java)) {
      result.set("item_detail", this.itemDetail as Serializable)
    } else {
      throw UnsupportedOperationException(NewsData::class.java.name +
          " must implement Parcelable or Serializable or must be an Enum.")
    }
    return result
  }

  public companion object {
    @JvmStatic
    public fun fromBundle(bundle: Bundle): NewsDetailFragmentArgs {
      bundle.setClassLoader(NewsDetailFragmentArgs::class.java.classLoader)
      val __itemDetail : NewsData?
      if (bundle.containsKey("item_detail")) {
        if (Parcelable::class.java.isAssignableFrom(NewsData::class.java) ||
            Serializable::class.java.isAssignableFrom(NewsData::class.java)) {
          __itemDetail = bundle.get("item_detail") as NewsData?
        } else {
          throw UnsupportedOperationException(NewsData::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        if (__itemDetail == null) {
          throw IllegalArgumentException("Argument \"item_detail\" is marked as non-null but was passed a null value.")
        }
      } else {
        throw IllegalArgumentException("Required argument \"item_detail\" is missing and does not have an android:defaultValue")
      }
      return NewsDetailFragmentArgs(__itemDetail)
    }

    @JvmStatic
    public fun fromSavedStateHandle(savedStateHandle: SavedStateHandle): NewsDetailFragmentArgs {
      val __itemDetail : NewsData?
      if (savedStateHandle.contains("item_detail")) {
        if (Parcelable::class.java.isAssignableFrom(NewsData::class.java) ||
            Serializable::class.java.isAssignableFrom(NewsData::class.java)) {
          __itemDetail = savedStateHandle.get<NewsData?>("item_detail")
        } else {
          throw UnsupportedOperationException(NewsData::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        if (__itemDetail == null) {
          throw IllegalArgumentException("Argument \"item_detail\" is marked as non-null but was passed a null value")
        }
      } else {
        throw IllegalArgumentException("Required argument \"item_detail\" is missing and does not have an android:defaultValue")
      }
      return NewsDetailFragmentArgs(__itemDetail)
    }
  }
}
