package com.example.osplabsassignment.ui

import android.os.Bundle
import android.os.Parcelable
import androidx.navigation.NavDirections
import com.example.osplabsassignment.R
import com.example.osplabsassignment.`data`.NewsData
import java.io.Serializable
import java.lang.UnsupportedOperationException
import kotlin.Int
import kotlin.Suppress

public class NewsFragmentDirections private constructor() {
  private data class ActionNewsFragmentToNewsDetailFragment(
    public val itemDetail: NewsData
  ) : NavDirections {
    public override val actionId: Int = R.id.action_newsFragment_to_newsDetailFragment

    public override val arguments: Bundle
      @Suppress("CAST_NEVER_SUCCEEDS")
      get() {
        val result = Bundle()
        if (Parcelable::class.java.isAssignableFrom(NewsData::class.java)) {
          result.putParcelable("item_detail", this.itemDetail as Parcelable)
        } else if (Serializable::class.java.isAssignableFrom(NewsData::class.java)) {
          result.putSerializable("item_detail", this.itemDetail as Serializable)
        } else {
          throw UnsupportedOperationException(NewsData::class.java.name +
              " must implement Parcelable or Serializable or must be an Enum.")
        }
        return result
      }
  }

  public companion object {
    public fun actionNewsFragmentToNewsDetailFragment(itemDetail: NewsData): NavDirections =
        ActionNewsFragmentToNewsDetailFragment(itemDetail)
  }
}
